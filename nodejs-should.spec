%{?nodejs_find_provides_and_requires}
%global enable_tests 1
Name:                nodejs-should
Version:             8.4.0
Release:             2
Summary:             A test framework agnostic BDD-style assertions for Node.js
License:             MIT
URL:                 https://github.com/shouldjs/should.js
Source0:             https://github.com/shouldjs/should.js/archive/%{version}.tar.gz
Obsoletes:           nodejs-should-promised < 0.3.1-2
BuildArch:           noarch
ExclusiveArch:       %{nodejs_arches} noarch
BuildRequires:       nodejs-packaging
%if 0%{?enable_tests}
BuildRequires:       npm(mocha) npm(bluebird) npm(should-equal) npm(should-format) npm(should-type)
%endif
Patch0001:           0001-Fix-flaky-tests.patch
%description
should is an expressive, readable, test framework agnostic, behavior-driven
development (BDD)-style assertion library for Node.js.
It extends the Object prototype with a single non-enumerable getter that
allows you to express how that object should behave.
should literally extends Node's assert module. For example,
should.equal(str, 'foo') will work, just as assert.equal(str, 'foo') would,
and should.AssertionError is assert.AssertionError, meaning any test framework
supporting this constructor will function properly with should.

%prep
%autosetup -n should.js-%{version} -p1
rm -f should.min.js
find . -type f -exec chmod -x '{}' \;

%build

%install
mkdir -p %{buildroot}%{nodejs_sitelib}/should
cp -pr package.json index.js lib/ should.js \
    %{buildroot}%{nodejs_sitelib}/should
%nodejs_symlink_deps
%if 0%{?enable_tests}

%check
%nodejs_symlink_deps --check
%{nodejs_sitelib}/mocha/bin/mocha --ui exports test/**/*.test.js
%endif

%files
%doc Readme.md History.md CONTRIBUTING.md
%license LICENSE
%{nodejs_sitelib}/should

%changelog
* Web 09 Jun 2021 zhaoyao<zhaoyao32@huawei.com> - 8.4.0-2
- %prep no longer patched with git 

* Thu Aug 20 2020 wangyue <wangyue92@huawei.com> - 8.4.0-1
- package init
